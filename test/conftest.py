
import pytest

@pytest.fixture
def auth_token(client):
    """Fixture to log in and return the authentication token."""
    response = client.post("/demo-room/start", json={'game_uuid': '73799b4a0b7211efb1714d17d770b423', 'player' : {'email': 'test@test.ch', 'name':'Test'}})
    token = response.json["token"]
    return token