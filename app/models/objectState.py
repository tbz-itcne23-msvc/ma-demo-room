from apiflask import Schema
from apiflask.fields import Boolean, String
from apiflask.validators import Length

from app.extensions import db

class ObjectStateOut(Schema):
    exits_locked = Boolean()

class ObjectState(db.Model):
    __tablename__ = 'object_states'
    id = db.Column(db.Integer, primary_key=True)
    visible = db.Column(db.Boolean())
    locked = db.Column(db.Boolean())
    state = db.Column(db.String(128))
    room_state_id = db.Column(db.Integer, db.ForeignKey("room_states.id"))
    room_state = db.relationship("RoomState", back_populates="object_states")
    object_id = db.Column(db.Integer, db.ForeignKey("objects.id"))
    object = db.relationship("Object")

    def __init__( self, visible, locked, state ):
        self.visible = visible
        self.locked = locked
        self.state = state