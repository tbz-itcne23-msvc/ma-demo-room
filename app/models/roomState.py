from apiflask import Schema
from apiflask.fields import Integer, String, Boolean, Nested
from apiflask.validators import Length, Email

from app.models.object import ObjectOut
from app.models.player import PlayerIn
from app.models.objectState import ObjectState

from flask import current_app, jsonify
from app.extensions import db
from jwt import encode, decode, InvalidTokenError

from datetime import datetime

import logging

####
## Schemas for OpenAPI and validation
###


class RoomStateOut(Schema):
    objects = Nested(ObjectOut(many=True))
    exits_locked = Boolean()
    message = String()
    game_uuid = String()

class RoomStateIn(Schema):
    player = Nested(PlayerIn)
    game_uuid = String(required=True)

class RoomState(db.Model):
    __tablename__ = 'room_states'
    id = db.Column(db.Integer, primary_key=True)
    started_on = db.Column(db.DateTime)
    game_uuid = db.Column(db.String(32))
    solved = db.Column(db.Boolean)
    exits_locked = db.Column(db.Boolean)
    object_states = db.relationship("ObjectState", back_populates="room_state")
    player_id = db.Column(db.Integer, db.ForeignKey("players.id"))
    player = db.relationship("Player")

    def __init__( self, game_uuid ):
        self.game_uuid = game_uuid
        self.started_on = datetime.now()
        self.solved = False
        self.exits_locked = True

    def generate_auth_token(self):
        return encode(
            { 'uuid': self.game_uuid },
            current_app.config['SECRET_KEY'], algorithm='HS256')

    def get_object_state_by_name(self, object_name):
        for object_state in self.object_states:
            if object_state.object.name == object_name:
                return object_state
        return None  


    def assemble_json_room_state(self, message):
        # assemble state to return to client
        objects = []
        for obj_state in self.object_states:
            if obj_state.visible:
                obj = {
                    "id": str(obj_state.object.id),
                    "name": str(obj_state.object.name),
                    "locked": str(obj_state.locked),
                    "state": str(obj_state.state),
                    "description": str(obj_state.object.description),
                    "icon": str(obj_state.object.icon)
                }
                objects.append(obj)

        json_data = {
            "objects": objects,
            "exits_locked": self.exits_locked,
            "game_uuid": self.game_uuid,
            "message": message
        }

        return jsonify(json_data)


    @staticmethod
    def verify_auth_token(token):
        try:
            data = decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])
            return RoomState.query.filter_by(game_uuid=data['uuid']).first()
        except InvalidTokenError:
            # Handle invalid token, if necessary
            logging.warning('Token was invalid')
            return None
        except Exception as e:
            # Log or handle other exceptions
            logging.warning('Unknown token validation error')
            return None
        
   