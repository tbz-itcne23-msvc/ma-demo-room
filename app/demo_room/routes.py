from app.demo_room import bp
from app.extensions import db
from app.models.roomState import RoomState, RoomStateIn, RoomStateOut
from app.models.player import Player
from app.models.object import Object
from app.models.objectState import ObjectState
from app.models.schemas import PingOut, DescriptionOut, ImageOut, ExitsOut, TokenOut, InteractionIn

from flask import current_app

from app.auth import token_auth


def create_room_objects():
    # Room Objects
    objects = [
        {
            'name': 'Wooden Desk', 
            'description': 'An old wooden desk with many drawers. One of the drawers has a key-hole in it.',
            'active':True,
            'icon':'archive-fill'
        },
        {
            'name': 'Column', 
            'description': 'Central to the room there is a stone column. The column has a three-digit combination lock on it.',
            'active':True,
            'icon':'database-lock'
        },
        {
            'name': 'Silver Key',
            'description': 'A worn key. You found it below the desk. The rests of the duct tape used to fix are still there.',
            'active':True,
            'icon':'key-fill'
        },
        {
            'name': 'Diary',
            'description': 'An old book with a bookmark inside. You wonder, what this great detective had to write.',
            'active':True,
            'icon':'journal'
        }
    ]

    # check if we already have objects in the DB
    cur_objs = Object.query.all()

    # if not create the objects initially
    if not cur_objs:
        for obj_data in objects:
            object = Object(**obj_data)
            db.session.add(object)
            db.session.commit()


####
## view functions
####    
@bp.get('/ping')
@bp.output(PingOut, status_code=200)
def get_ping():
    return {"message":"pong"}

@bp.get('/description')
@bp.output(DescriptionOut, status_code=200)
def get_description():
    return {
            "description":
            """The demo room of the Micro Adventures Game is a cosy old office. 
            Maybe it was once the office of a private detective."""
    }

@bp.get('/image')
@bp.output(ImageOut, status_code=200)
def get_image():
    return current_app.send_static_file('demo_room.png')

@bp.get('/exits')
@bp.output(ExitsOut, status_code=200)
def get_exits():
    # descbribe the exits as seen from outside the room
    return {
            "north": "A sunny path leads to a light door",
            "east": "A heavy wooden door with the sign 'Demo' on it",
            "south": "A glass door that reveals the friendly demo room behind it.",
            "west": "An arrow-sign on the path reads 'Demo-Room'"
    }

@bp.post('/start')
@bp.input(RoomStateIn, location='json')
@bp.output(TokenOut, status_code=200)
def start_visit(json_data):
    # Get the player email from the nested JSON data
    player_email = json_data.get('player', {}).get('email')
    # search player by email in database (TODO: does not work - each time a new player is generated)
    player = Player.query.filter_by(email=player_email).first()

    # if player does not exist, create a db entry
    if not player:
        player = Player(**json_data.get('player'))
        db.session.add(player)
    
    # create a roomState object for the current visit
    roomState = RoomState(json_data.get('game_uuid'))
    roomState.player = player
        
    # create states for all objects
    objects = Object.query.all()
    visibility = False
    locked = False
    for obj in objects:
        match obj.name:
            case 'Wooden Desk':
                visibility = True
                locked = True
            case 'Column':
                visibility = True
                locked = True
            case 'Silver Key':
                visibility = False
                locked = False
            case 'Diary':
                visibility = False
                locked = False
            case _:
                visibility = False
                locked = False

        objState = ObjectState(visibility, locked, "untouched")
        objState.object = obj
        roomState.object_states.append(objState)

    # generate a token to access this room during a server-game (should be stored in game server)
    token = roomState.generate_auth_token()

    db.session.add(roomState)
    db.session.commit()

    return {'token':token}


@bp.get('/examine')
@bp.auth_required(token_auth)
@bp.output(RoomStateOut, status_code=200)
def examine_room():
    # get room_state corresponding to token
    room_state = token_auth.current_user
   
    return room_state.assemble_json_room_state("nothing new")


@bp.get('/objects/<int:object_id>/examine')
@bp.auth_required(token_auth)
@bp.output(RoomStateOut, status_code=200)
def examine_object(object_id):
    # get room_state corresponding to token
    room_state = token_auth.current_user
    # get current state of given object
    current_object = db.get_or_404(Object, object_id)
    object_state = room_state.get_object_state_by_name(current_object.name)

    # GAME_LOGIC
    message = 'Nothing new'
    match current_object.name:
        case 'Wooden Desk':
            if object_state.state == 'untouched':
                object_state.state = 'examined'
                silver_key_state = room_state.get_object_state_by_name('Silver Key')
                silver_key_state.visible = True
                message = 'You found a silver key attached with a duct tape under the desk.'
        case 'Diary':
            message = 'At the page of the diary where the bookmark is located, there is a hint: todays exit-combination is 1234.'
        case _:
            message = 'You did not find anything new.'

    # update DB and return state to client
    db.session.commit()
    return room_state.assemble_json_room_state(message)


@bp.post('/objects/<int:object_id>/interact')
@bp.auth_required(token_auth)
@bp.input(InteractionIn, location='json')
@bp.output(RoomStateOut, status_code=200)
def interact_with_object(object_id, json_data):
    # get room_state corresponding to token
    room_state = token_auth.current_user
    # get current state of given object
    current_object = db.get_or_404(Object, object_id)
    object_state = room_state.get_object_state_by_name(current_object.name)
    
    # GAME_LOGIC
    message = ''
    match current_object.name:
        case 'Column':
            if json_data.get('parameter'):
                param_string = json_data.get('parameter')
                message = 'You enter a code into the column, but it seems to be wrong.'
                if param_string == '1234':
                    room_state.exits_locked = False
                    object_state.state = "unlocked"
                    message = 'Congratulations. You solved this room. The exit doors snap open.'
        case 'Wooden Desk':
            if json_data.get('object_id'):
                param_object = db.get_or_404(Object, json_data.get('object_id'))
                # if the user used the right key...
                if param_object.name == 'Silver Key':
                    # the desk gets unlocked
                    object_state.locked = False
                    # and the player finds a diary in it 
                    diary_state = room_state.get_object_state_by_name('Diary')
                    diary_state.visible = True
                    # and the key vanishes from the list since it was used
                    silver_key_state = room_state.get_object_state_by_name('Silver Key')
                    silver_key_state.visible = False
                    message = 'You used the silver key to unlock the desk drawer and found a diary'
        case _:
            message = 'Nothing happened.'

    # update DB and return state to client
    db.session.commit()
    return room_state.assemble_json_room_state(message)